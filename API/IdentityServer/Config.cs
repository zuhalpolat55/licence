﻿using IdentityServer4.Models;
using System.Collections.Generic;

namespace IdentityServer
{
    static public class Config
    {
        public static IEnumerable<ApiScope> GetApiScopes()
        {
            return new List<ApiScope>
            {
                new ApiScope("Licence.Write","Licence Write"),
                new ApiScope("Licence.Read","Licence Read"),
            };
        }

        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource("Licence"){ Scopes = { "Licence.Write", "Licence.Read" } },
            };
        }

        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>
            {
                new Client
                {
                    ClientId = "LicenceService",
                    ClientName = "LicenceService",
                    ClientSecrets = { new Secret("licence".Sha256()) },
                    AllowedGrantTypes = { GrantType.ClientCredentials },
                    AllowedScopes = { "Licence.Write", "Licence.Read" }
                }
            };
        }
    }
}