﻿using API.DTOs;
using API.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<ProcessDataDTO, ProcessData>().ReverseMap();
            CreateMap<IProcessDataDTO, ProcessData>().ReverseMap();
            CreateMap<ProcessDTO, Processes>().ReverseMap();
        }
    }
}
