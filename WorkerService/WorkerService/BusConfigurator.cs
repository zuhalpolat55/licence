using MassTransit;
using MassTransit.RabbitMqTransport;
using System;
using System.Collections.Generic;
using System.Text;

namespace WorkerService
{
    public static class BusConfigurator
    {
        public static IBusControl ConfigureBus(Action<IRabbitMqBusFactoryConfigurator> registrationAction = null)
        {
            return Bus.Factory.CreateUsingRabbitMq(cfg =>
             {
                 cfg.Host(new Uri(RabbitMqConsts.RabbitMqUri), hst =>
                 {
                    hst.Username(RabbitMqConsts.Username);
                    hst.Password(RabbitMqConsts.Password);
                 });
               
                 registrationAction?.Invoke(cfg);
             });
        }
    }

}